package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.exarecc2.Imc;

import java.util.ArrayList;

public class ImcDb implements Persistencia, Proyeccion{

    private Context context;
    private ImcDbHelper helper;
    private SQLiteDatabase db;

    public ImcDb(Context context, ImcDbHelper helper){
        this.context=context;
        this.helper=helper;
    }

    public ImcDb(Context context){
        this.context=context;
        this.helper=new ImcDbHelper(this.context);
    }


    @Override
    public void openDataBase() {
        db=helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    public long insertImc(Imc imc) {

        ContentValues values=new ContentValues();

        values.put(DefineTabla.Imcs.COLUMN_NAME_ALTURA, imc.getTextAltura());
        values.put(DefineTabla.Imcs.COLUMN_NAME_PESO, imc.getTextPeso());
        values.put(DefineTabla.Imcs.COLUMN_NAME_IMC, imc.getTextIMC());

        this.openDataBase();
        long num = db.insert(DefineTabla.Imcs.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar","insertImc: "+num);

        return num;
    }

    @Override
    public long updateImc(Imc imc) {
        ContentValues values=new ContentValues();

        values.put(DefineTabla.Imcs.COLUMN_NAME_ALTURA, imc.getTextAltura());
        values.put(DefineTabla.Imcs.COLUMN_NAME_PESO, imc.getTextPeso());
        values.put(DefineTabla.Imcs.COLUMN_NAME_IMC, imc.getTextIMC());

        this.openDataBase();
        long num = db.update(
                DefineTabla.Imcs.TABLE_NAME,
                values,
                DefineTabla.Imcs.COLUMN_NAME_ID+"="+imc.getId(),
                null);
        this.closeDataBase();
        Log.d("agregar","insertImco: "+num);

        return num;
    }

    @Override
    public void deleteImcs(int id) {
        this.openDataBase();
        db.delete(
                DefineTabla.Imcs.TABLE_NAME,
                DefineTabla.Imcs.COLUMN_NAME_ID+"=?",
                new String[] {String.valueOf(id)});
        this.closeDataBase();
    }

    public void deleteAllImcs() {
        this.openDataBase();
        db.delete(DefineTabla.Imcs.TABLE_NAME, null, null);
        this.closeDataBase();
    }


    @Override
    public Imc getImc(String id) {
        db = helper.getWritableDatabase();

        String stringId = String.valueOf(id);

        Cursor cursor = db.query(
                DefineTabla.Imcs.TABLE_NAME,
                DefineTabla.Imcs.REGISTRO,
                DefineTabla.Imcs.COLUMN_NAME_ID + " = ?",
                new String[]{stringId},
                null, null, null);

        if (cursor.moveToFirst()) {
            Imc imc = readImc(cursor);
            cursor.close();
            return imc;
        } else {
            cursor.close();
            return null; // El registro no se encontró en la base de datos
        }
    }

    @Override
    public ArrayList<Imc> allImcs() {
        db=helper.getWritableDatabase();

        Cursor cursor=db.query(
                DefineTabla.Imcs.TABLE_NAME,
                DefineTabla.Imcs.REGISTRO,
                null, null, null, null, null);
        ArrayList<Imc> imcs = new ArrayList<Imc>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            Imc imc=readImc(cursor);
            imcs.add(imc);
            cursor.moveToNext();
        }

        cursor.close();
        return imcs;
    }

    @Override
    public Imc readImc(Cursor cursor) {
        Imc imc=new Imc();

        imc.setId(cursor.getInt(0));
        imc.setTextAltura(cursor.getFloat(1));
        imc.setTextPeso(cursor.getFloat(2));
        imc.setTextIMC(cursor.getFloat(3));

        return imc;
    }
}

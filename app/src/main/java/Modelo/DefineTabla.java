package Modelo;

public class DefineTabla {

    public DefineTabla(){}

    public static abstract class Imcs{
        public static final String TABLE_NAME="imc";
        public static final String COLUMN_NAME_ID="id";
        public static final String COLUMN_NAME_ALTURA="altura";
        public static final String COLUMN_NAME_PESO="peso";
        public static final String COLUMN_NAME_IMC="imc";

        public static String[] REGISTRO = new String[]{
                Imcs.COLUMN_NAME_ID,
                Imcs.COLUMN_NAME_ALTURA,
                Imcs.COLUMN_NAME_PESO,
                Imcs.COLUMN_NAME_IMC
        };
    }
}



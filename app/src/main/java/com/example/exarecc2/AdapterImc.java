package com.example.exarecc2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterImc extends BaseAdapter {
    private List<Imc> datos;
    private Context context;

    public AdapterImc(Context context, List<Imc> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int position) {
        return datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            // Inflar el diseño personalizado del elemento de la lista
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.imc_item, parent, false);

            // Crear un ViewHolder para mantener las referencias a las vistas
            viewHolder = new ViewHolder();
            viewHolder.lblIdHis = convertView.findViewById(R.id.lblIdHis);
            viewHolder.lblAlturaHis = convertView.findViewById(R.id.lblAlturaHis);
            viewHolder.lblPesoHis = convertView.findViewById(R.id.lblPesoHis);
            viewHolder.lblIMCHis = convertView.findViewById(R.id.lblIMCHis);

            // Establecer el ViewHolder como una etiqueta de la vista convertida
            convertView.setTag(viewHolder);
        } else {
            // Si convertView no es nulo, obtener el ViewHolder de la etiqueta
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Obtener el elemento de datos para la posición actual
        Imc imc = datos.get(position);

        // Establecer los datos en las vistas del diseño personalizado
        viewHolder.lblIdHis.setText(String.valueOf(imc.getId()));
        viewHolder.lblAlturaHis.setText(String.valueOf(imc.getTextAltura()));
        viewHolder.lblPesoHis.setText(String.valueOf(imc.getTextPeso()));
        viewHolder.lblIMCHis.setText(String.valueOf(imc.getTextIMC()));

        return convertView;
    }

    private static class ViewHolder {
        TextView lblIdHis;
        TextView lblAlturaHis;
        TextView lblPesoHis;
        TextView lblIMCHis;
    }
}

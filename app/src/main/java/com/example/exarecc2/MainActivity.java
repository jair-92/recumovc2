package com.example.exarecc2;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText txtAltura;
    private EditText txtPeso;
    private TextView lblIMC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Enlazar vistas con las variables
        TextView lblTitulo = findViewById(R.id.lblTitulo);
        TextView lblAltura = findViewById(R.id.lblAltura);
        TextView lblPeso = findViewById(R.id.lblPeso);
        lblIMC = findViewById(R.id.lblIMC);
        txtAltura = findViewById(R.id.txtAltura);
        txtPeso = findViewById(R.id.txtPeso);
        Button btnCalcular = findViewById(R.id.btnCalcular);
        Button btnGuardar = findViewById(R.id.btnGuardar);
        Button btnVerHistorial = findViewById(R.id.btnVerHistorial);
        Button btnSalir = findViewById(R.id.btnSalir);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularIMC();
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // PENDIENTE
            }
        });

        btnVerHistorial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HistorialActivity.class);
                startActivity(intent);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularIMC() {
        // Obtener los valores de altura y peso
        String alturaStr = txtAltura.getText().toString();
        String pesoStr = txtPeso.getText().toString();

        if (!alturaStr.isEmpty() && !pesoStr.isEmpty()) {
            // Convertir los valores a números
            double altura = Double.parseDouble(alturaStr);
            double peso = Double.parseDouble(pesoStr);

            // Calcular el índice de masa corporal (IMC)
            double imc = peso / (altura * altura);

            // Mostrar el resultado en el TextView lblIMC
            lblIMC.setText("Resultado IMC: " + imc);
        } else {
            Toast.makeText(this, "Por favor ingresa altura y peso", Toast.LENGTH_SHORT).show();
        }
    }
}
